# lora-transformer-ui/frontend

This is the directory for your web application. All web assets are expected to reside in the `dist` folder and
are required by the `backend` project. If you change the name of the folder then you must also affect the change
in just one place of the `backend/pom.xml` file.